package org.alekceu.hangman.model;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class Hangman {

    private String word;
    private int attempts;
    private String letter;
    private String guess;
}